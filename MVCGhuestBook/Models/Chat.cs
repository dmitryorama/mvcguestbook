﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCGuestBook
{
    public class Chat
    {
        public string History { get; set; }
        
        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Сообщение")]
        [StringLength(10)]
        public string Message { get; set; }

    }
}